import React, { Component } from 'react';
import { HashRouter, Route, Link } from 'react-router-dom'
import Homepage from './views/Homepage.jsx';
import Movie from './views/Movie.jsx';

class App extends Component {
    render() {
        return (
            <div>
                <Homepage />
            </div>
        );
    }
}
export default App;