import React, { Component } from 'react';
import FilterBy from '../components/Navbar/FilterBy.jsx';
import SearchBar from '../components/Searchbar/SearchBar.jsx';
import Movies from '../components/Movie/Movies.jsx';



class Homepage extends Component {
    render() {
        return (
            <div>
                <FilterBy />
                <SearchBar />
            </div>
        );
    }
}

export default Homepage;