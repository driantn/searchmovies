import React, { Component } from 'react';
import axios from 'axios';
import { Item, Label, Container } from 'semantic-ui-react';
require('../components/Movie/movie.scss');

class Movie extends Component {

    constructor(props) {
        super(props);
        this.state = {
            movie: []
        }
    }

    componentDidMount() {
        axios.get('http://www.omdbapi.com/?i=' + this.props.match.params.movieId + '&type=movie&plot=full&y=yes&apikey=ae6969ba')
            .then((response) => {
                if (response.data.Response === "True") {
                    this.setState({ movie: response.data });
                }
            }).catch(function (error) {
                console.log(error);
            });;
    }

    render() {
        let url = 'http://www.imdb.com/title/' + this.state.movie.imdbID;
        return (
            <Container className="single-movie">
                <Item.Group>
                    <Item>
                        <Item.Image size='large' src={this.state.movie.Poster} />
                        <Item.Content>
                            <Item.Header as='a'>{this.state.movie.Title}, {this.state.movie.Year}</Item.Header>
                            <Item.Description>
                                <p>{this.state.movie.Plot}</p>
                            </Item.Description>
                            <Item.Extra>
                                <Label size="big" color='orange' as="a" href={url} target="_blank">
                                    IMDB
                        </Label>
                            </Item.Extra>
                        </Item.Content>
                    </Item>
                </Item.Group>
            </Container>
        );
    }
}

export default Movie;