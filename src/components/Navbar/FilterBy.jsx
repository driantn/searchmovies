import React, { Component } from 'react';
import { Menu, Segment, Header, Button } from 'semantic-ui-react';
require('./filterBy.scss');

class FilterBy extends Component {
    constructor(props) {
        super(props);
        this.state = { activeItem: localStorage.getItem('filterType') !== null ? localStorage.getItem('filterType') : 'movie' };
    }

    handleItemClick = (e, { name }) => {
        this.setState({ activeItem: name });
        localStorage.setItem('filterType', name);
    }

    render() {
        const { activeItem } = this.state;
        return (
            <Segment raised textAlign='center'>
                <Header as='h5'>Filter by:</Header>
                <Menu fluid text className="filter-by">
                    <Menu.Item name='movie' color={activeItem === 'movie' ? 'green' : 'grey'} active={activeItem === 'movie'} onClick={this.handleItemClick} />
                    <Menu.Item name='series' color={activeItem === 'series' ? 'green' : 'grey'} active={activeItem === 'series'} onClick={this.handleItemClick} />
                    <Menu.Item name='episode' color={activeItem === 'episode' ? 'green' : 'grey'} active={activeItem === 'episode'} onClick={this.handleItemClick} />
                </Menu>
            </Segment>
        )
    }
}

export default FilterBy;