import React, { Component } from 'react';
import { Segment, Button, Form, Container, Label, Popup } from 'semantic-ui-react';
import axios from 'axios';
import Movies from '../Movie/Movies.jsx';
require('./searchBar.scss');

class SearchBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            value: '',
            movies: []
        };
    }
    handleFormSubmit = (e) => {
        e.preventDefault();
        let self = this;
        if (self.state.value === "") {
            return false;
        }
        self.setState({ isLoading: true });
        var filterType = localStorage.getItem('filterType') !== null ? localStorage.getItem('filterType') : 'movie';
        axios.get('http://www.omdbapi.com/?s=' + self.state.value + '&type=' + filterType + '&plot=short&apikey=ae6969ba')
            .then(function (response) {
                // added 3 sec dilay to show loading state
                setTimeout(() => {
                    if (response.data.Response === 'True') {
                        self.setState({ movies: response.data.Search });
                    } else {
                        self.setState({ movies: [] });
                    }
                    self.setState({ isLoading: false });
                }, 3 * 1000);
            })
            .catch(function (error) {
                console.log(error);
                self.setState({ isLoading: false });
            });
    }


    handleSearchChange = (e, data) => {
        this.setState({ value: data.value });
    }

    render() {
        return (
            <Container>
                <Form className="search-form" onSubmit={this.handleFormSubmit.bind(this)}>
                    <Form.Group>
                        <Form.Input size='massive' color='green' placeholder='search' disabled={this.state.isLoading} readOnly={this.state.isLoading} onChange={this.handleSearchChange} />
                        <Button basic type='submit' color='green' loading={this.state.isLoading} disabled={this.state.isLoading}>Search</Button>
                    </Form.Group>
                </Form>
                <Movies movies={this.state.movies} />
            </Container>
        );
    }
}

export default SearchBar;