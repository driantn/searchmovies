import React, { Component } from 'react';
import MovieItem from './MovieItem.jsx';
import { Container, Card, Message, Label } from 'semantic-ui-react';
require('./movies.scss');

class Movies extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let movieList = this.props.movies.map((movie) => {
            return <MovieItem key={movie.imdbID} movie={movie} />;
        });
        return (
            <Container className="movies-list">
                {
                    movieList.length
                        ?
                        <div>
                            <Label as='span' color='teal' tag>{movieList.length} Movies Found</Label>
                            <Card.Group>
                                {movieList}
                            </Card.Group>
                        </div>
                        :
                        <Message
                            info
                            icon='search'
                            header='Movie list is empty'
                            content=''
                        />
                }

            </Container>
        );
    }
}

export default Movies;