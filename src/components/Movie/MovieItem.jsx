import React, { Component } from 'react';
import { Card, Icon, Image, Label } from 'semantic-ui-react';
import { Link } from 'react-router-dom';


class MovieItem extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let url = '/movie/' + this.props.movie.imdbID;
        return (
            <Card raised>
                <Image src={this.props.movie.Poster} />
                <Card.Content>
                    <Card.Header>
                        {this.props.movie.Title}
                    </Card.Header>
                    <Card.Meta>
                        <span className='date'>
                            {this.props.movie.Year}
                        </span>
                    </Card.Meta>
                    <Card.Description>
                        <Link to={url}>Show Details</Link>
                        {/*<Label size="big" color='orange' as="a" href={url} target="_blank">
                            Show Details
                        </Label>*/}
                    </Card.Description>
                </Card.Content>
            </Card >
        );
    }
}

export default MovieItem;