// require("./styles/style.scss");

// Render the top-level React component
import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter, Route, Link } from 'react-router-dom'
import App from './App.jsx';
import Movie from './views/Movie.jsx'

ReactDOM.render(<HashRouter>
    <div>
        <Route exact path='/' component={App} />
        <Route path='/movie/:movieId' component={Movie} />
    </div>
</HashRouter>, document.getElementById('react-app'));